import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import * as cors from 'cors';
const corsHandler = cors({ origin: true });

admin.initializeApp();
let defaultStorage = admin.storage();

export const probar = functions.https.onRequest((req, res) => {
    corsHandler(req, res, () => {
        let requestedUid = req.body.uid
        let authToken = validateHeader(req)

        if (!authToken) {
            return res.status(403).send('{ "mensaje" : "No tienes los permisos"}');
        }

        return decodeAuthToken(authToken)
            .then(uid => {
                console.log('UID ' + uid);
                if (uid === requestedUid) {
                    return res.status(200).send('{ "mensaje" : "Hola desde cors"}');
                } else {
                    return res.status(403).send('{ "mensaje" : "No tienes los permisos"}');
                }
            })
            .catch(err => console.log(err));
        //console.log('DEPURANDO!!!!');        
    });
});

function validateHeader(req: any) {
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
        return req.headers.authorization.split('Bearer ')[1]
    }
}

function decodeAuthToken(authToken: any) {
    return admin.auth()
        .verifyIdToken(authToken)
        .then(decodedToken => {
            return decodedToken.uid;
        });
}

export const removerFoto = functions.firestore
    .document('platos/{id}')
    .onDelete((snap, context) => {
        const id = context.params.id;
        console.log(`IDSTORAGE: ${id}`);

        const bucket = defaultStorage.bucket();
        const file = bucket.file(`platos/${id}`);

        return file.delete();
    });