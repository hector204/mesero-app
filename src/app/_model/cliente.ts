export class Cliente {
    id: string;
    dni: string;
    nombreCompleto: string;
    edad: number;
}