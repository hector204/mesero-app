import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Consumo } from './../_model/consumo';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  constructor(private afs: AngularFirestore) { }

  listar(fecha: Date) {
    let inicio = moment(fecha)/*.toISOString();*//*.subtract(1, 'days')*/.format('YYYY-MM-DD');
    let fin = moment(inicio).add(1, 'days')/*.toISOString();*/.format('YYYY-MM-DD');

    return this.afs.collection('consumos', ref =>
      ref.where('fechaPedido', '>=', new Date(inicio))
        .where('fechaPedido', '<', new Date(fin))
    ).snapshotChanges();
  }

}
