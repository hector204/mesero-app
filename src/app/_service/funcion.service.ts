import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class FuncionService {

  url: string = 'https://us-central1-mesero-app-demo-backend.cloudfunctions.net/probar';

  constructor(private http: HttpClient,
    public afa: AngularFireAuth) { }

  probar() {
    this.afa.auth.currentUser.getIdToken().then(authToken => {

      const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + authToken });
      const objUser = { uid: this.afa.auth.currentUser.uid };

      return this.http.post(this.url, objUser, { headers: headers })
        .toPromise().then(res =>
          console.log(res)
        );
    });
  }
}
