import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Subject } from 'rxjs';
import { Menu } from '../_model/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  menu = new Subject<Menu[]>();

  constructor(private afs: AngularFirestore) { }

  listar() {
    return this.afs.collection<Menu>('menus').valueChanges();
  }
}
