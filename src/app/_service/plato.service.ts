import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Plato } from '../_model/plato';

@Injectable({
  providedIn: 'root'
})
export class PlatoService {

  constructor(
    private fs: AngularFirestore
  ) { }

  listar() {
    return this.fs.collection<Plato>('platos').valueChanges();
  }

  registrar(plato: Plato) {
    return this.fs.collection('platos').doc(plato.id).set({
      id: plato.id,
      nombre: plato.nombre,
      precio: plato.precio
    });
  }

  leer(id: string) {
    return this.fs.collection<Plato>('platos').doc(id).valueChanges();
  }

  modificar(plato: Plato) {
    return this.fs.collection('platos').doc(plato.id).set(JSON.parse(JSON.stringify(plato)));
  }

  eliminar(plato: Plato) {
    return this.fs.collection('platos').doc(plato.id).delete();
  }

}
