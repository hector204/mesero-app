import { LoginService } from './_service/login.service';
import { Component, OnInit } from '@angular/core';
import { MenuService } from './_service/menu.service';
import { Menu } from './_model/menu';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  private ngUnsubscribe: Subject<void> = new Subject();
  menus: Menu[] = [];

  constructor(public loginService: LoginService
    , private menuService: MenuService) {
  }

  ngOnInit() {
    this.menuService.menu.pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
      this.menus = data;
    });
  }

}
