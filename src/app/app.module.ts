import { OnlyLettersDirective } from './_directives/onlyletters.directive';
import { OnlyNumbersDirective } from './_directives/onlynumbers.directive';

import { environment } from './../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { PlatoComponent } from './pages/plato/plato.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireStorageModule } from '@angular/fire/storage';
import { FirestoreSettingsToken } from '@angular/fire/firestore';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlatoEdicionComponent } from './pages/plato/plato-edicion/plato-edicion.component';
import { PlatoListaComponent } from './pages/plato/plato-lista/plato-lista.component';
import { Not404Component } from './pages/not404/not404.component';
import { ConsumoComponent } from './pages/consumo/consumo.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { ClienteComponent } from './pages/cliente/cliente.component';

import * as moment from 'moment';
import { DateTimeFormatPipe } from './_pipe/dateTimeFormatPipe';
import { DialogoComponent } from './pages/consulta/dialogo/dialogo.component';
import { LoginComponent } from './login/login.component';

import { AngularFireAuthModule } from '@angular/fire/auth';
import { Not403Component } from './pages/not403/not403.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { ReporteComponent } from './pages/reporte/reporte.component';

import { HttpClientModule } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ClienteListaComponent } from './pages/cliente/cliente-lista/cliente-lista.component';
import { ClienteEdicionComponent } from './pages/cliente/cliente-edicion/cliente-edicion.component';

@NgModule({
  declarations: [
    AppComponent,
    PlatoComponent,
    PlatoEdicionComponent,
    PlatoListaComponent,
    Not404Component,
    ConsumoComponent,
    ConsultaComponent,
    ClienteComponent,
    DateTimeFormatPipe,
    DialogoComponent,
    LoginComponent,
    Not403Component,
    PerfilComponent,
    ReporteComponent,
    ClienteListaComponent,
    ClienteEdicionComponent,
    OnlyNumbersDirective,
    OnlyLettersDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFireAuthModule,
    HttpClientModule
  ],
  providers: [
    AngularFirestore,
    { provide: FirestoreSettingsToken, useValue: {} },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
