import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import { Cliente } from './../../../_model/cliente';
import { ClienteService } from './../../../_service/cliente.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-cliente-edicion',
  templateUrl: './cliente-edicion.component.html',
  styleUrls: ['./cliente-edicion.component.css']
})
export class ClienteEdicionComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject();

  form: FormGroup;
  id: string;
  edicion: boolean;

  labelFile: string;
  file: any;
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  urlImagen: string;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private clienteService: ClienteService,
    private snackBar: MatSnackBar,
    private afStorage: AngularFireStorage,
    private afs: AngularFirestore) { }

  ngOnInit() {
    this.form = new FormGroup({
      'id': new FormControl(''),
      'nombreCompleto': new FormControl(''),
      'dni': new FormControl(''),
      'edad': new FormControl('')
    });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = this.id != null;
      this.initForm();
    });
  }

  initForm() {
    if (this.edicion) {
      this.clienteService.leer(this.id).pipe(takeUntil(this.ngUnsubscribe)).subscribe((data: Cliente) => {
        this.form = new FormGroup({
          'id': new FormControl(data.id),
          'nombreCompleto': new FormControl(data.nombreCompleto),
          'dni': new FormControl(data.dni),
          'edad': new FormControl(data.edad)
        });
        if (data.id != null) {
          this.afStorage.ref(`clientes/${data.id}`).getDownloadURL().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
            if (data) {
              this.urlImagen = data;
            }
          });
        }
      });
    }
  }

  operar() {
    let nuevoCliente = new Cliente();
    nuevoCliente.id = this.id;
    nuevoCliente.nombreCompleto = this.form.value['nombreCompleto'];
    nuevoCliente.dni = this.form.value['dni'];
    nuevoCliente.edad = this.form.value['edad'];
    let mensaje = '';

    if (this.edicion) {
      this.clienteService.actualizar(nuevoCliente);
      mensaje = 'SE MODIFICO';
    } else {
      nuevoCliente.id = this.afs.createId();
      this.clienteService.registrar(nuevoCliente);
      mensaje = 'SE REGISTRO';
    }

    //se guarda la imagen en storage
    if (this.file != null) {
      this.ref = this.afStorage.ref(`clientes/${nuevoCliente.id}`);
      this.task = this.ref.put(this.file);
      this.afStorage.ref
    }

    this.snackBar.open(mensaje, 'AVISO', {
      duration: 2000,
    });

    this.router.navigate(['cliente']);

  }

  seleccionar(e: any) {
    this.file = e.target.files[0];
    this.labelFile = e.target.files[0].name;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
