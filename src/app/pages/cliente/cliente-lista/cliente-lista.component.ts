import { ClienteService } from './../../../_service/cliente.service';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Cliente } from './../../../_model/cliente';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-cliente-lista',
  templateUrl: './cliente-lista.component.html',
  styleUrls: ['./cliente-lista.component.css']
})
export class ClienteListaComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject();

  clientes: Cliente[] = [];
  dataSource: MatTableDataSource<Cliente>;
  displayedColumns = ['nombreCompleto', 'dni', 'edad', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private clienteService: ClienteService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.clienteService.listarValues().pipe(takeUntil(this.ngUnsubscribe)).subscribe(response => {
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }


  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
