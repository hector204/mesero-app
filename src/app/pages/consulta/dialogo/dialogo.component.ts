import { AngularFireStorage } from '@angular/fire/storage';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Consumo } from './../../../_model/consumo';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html',
  styleUrls: ['./dialogo.component.css']
})
export class DialogoComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject();

  consumo: Consumo;
  urlImagen: string;

  constructor(
    private afStorage: AngularFireStorage,
    private dialogRef: MatDialogRef<DialogoComponent>,
    @Inject(MAT_DIALOG_DATA) private data: Consumo
  ) { }

  ngOnInit() {
    if (this.data.cliente.id) {
      this.afStorage.ref(`clientes/${this.data.cliente.id}`).getDownloadURL().pipe(takeUntil(this.ngUnsubscribe)).subscribe(response => {
        if (response) {
          this.urlImagen = response;
        }
      });
    }
    this.consumo = this.data;
  }

  cerrar() {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
