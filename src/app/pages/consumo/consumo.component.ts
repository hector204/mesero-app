import { AngularFirestore } from '@angular/fire/firestore';
import { Consumo } from './../../_model/consumo';
import { ConsumoService } from './../../_service/consumo.service';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Detalle } from './../../_model/detalle';
import { PlatoService } from './../../_service/plato.service';
import { Plato } from './../../_model/plato';
import { Cliente } from './../../_model/cliente';
import { ClienteService } from './../../_service/cliente.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-consumo',
  templateUrl: './consumo.component.html',
  styleUrls: ['./consumo.component.css']
})
export class ConsumoComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject();

  form: FormGroup;
  myControlCliente: FormControl = new FormControl();
  myControlPlato: FormControl = new FormControl();

  cliente: Cliente;
  clientes: Cliente[];
  filteredClientes: Observable<any[]>;

  plato: Plato;
  platos: Plato[];
  filteredPlatos: Observable<any[]>;

  cantidad: number;
  detalle: Detalle[] = [];
  total: number = 0;

  dataSource: MatTableDataSource<Detalle>
  displayedColumns = ['nombre', 'precio', 'cantidad', 'subtotal', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private snackBar: MatSnackBar,
    private builder: FormBuilder,
    private clienteService: ClienteService,
    private platoService: PlatoService,
    private consumoService: ConsumoService,
    private afs: AngularFirestore

  ) { }

  ngOnInit() {
    this.form = this.builder.group({
      'cliente': this.myControlCliente,
      'plato': this.myControlPlato,
      'fecha': new FormControl(new Date()),
      'cantidad': new FormControl(0),
    });

    this.listarClientes();
    this.listarPlatos();
    this.filteredClientes = this.myControlCliente.valueChanges.pipe(map(val => this.filterClientes(val)));
    this.filteredPlatos = this.myControlPlato.valueChanges.pipe(map(val => this.filterPlatos(val)));
  }

  listarClientes() {
    this.clienteService.listarValues().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
      this.clientes = data;
    });
  }

  listarPlatos() {
    this.platoService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
      this.platos = data;
    });
  }

  filterClientes(val: any) {
    if (val != null && val.dni != null) {
      return this.clientes.filter(option =>
        option.nombreCompleto.toLowerCase().includes(val.nombreCompleto.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.clientes.filter(option =>
        option.nombreCompleto.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  filterPlatos(val: any) {
    if (val != null && val.nombre != null) {
      return this.platos.filter(option =>
        option.nombre.toLowerCase().includes(val.nombre.toLowerCase()));
    } else {
      return this.platos.filter(option =>
        option.nombre.toLowerCase().includes(val.toLowerCase()));
    }
  }

  displayFnCliente(val: Cliente) {
    return val ? `${val.nombreCompleto}` : val;
  }

  displayFnPlato(val: Plato) {
    return val ? `${val.nombre}` : val;
  }

  seleccionarCliente(e: any) {
    this.cliente = e.option.value;
  }

  seleccionarPlato(e: any) {
    this.plato = e.option.value;
  }

  agregar() {
    let det = new Detalle();
    det.plato = this.plato;
    det.cantidad = this.cantidad;
    this.detalle.push(det);

    this.total += det.plato.precio * det.cantidad;

    this.dataSource = new MatTableDataSource(this.detalle);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  remover(det: Detalle) {
    this.total -= det.plato.precio * det.cantidad;

    let indices = [];
    for (let i = 0; i < this.detalle.length; i++) {
      indices.push(this.detalle[i].index);
    }
    let index = indices.indexOf(det.index);

    this.detalle.splice(index, 1);

    this.dataSource = new MatTableDataSource(this.detalle);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  aceptar() {
    let consumo = new Consumo();
    consumo.detalle = this.detalle;
    consumo.fechaPedido = new Date();
    consumo.total = this.total;

    if (this.cliente == undefined) {
      this.cliente = new Cliente();
      this.cliente.id = this.afs.createId();
      this.cliente.nombreCompleto = this.form.value['cliente'];
      this.cliente.dni = '0000000';
      this.cliente.edad = 10;

      this.clienteService.registrar(this.cliente).then(data => {
        consumo.cliente = this.cliente;
        this.consumoService.registrar(consumo).then(() => {
          this.emitirMensaje();
        });
      });

    } else {
      //delete this.cliente.edad
      consumo.cliente = this.cliente;
      this.consumoService.registrar(consumo).then(() => {
        this.emitirMensaje();
      });
    }

  }

  aceptarTransaccion() {
    let consumo = new Consumo();
    consumo.detalle = this.detalle;
    consumo.fechaPedido = new Date();
    consumo.total = this.total;

    if (this.cliente === undefined) {
      this.cliente = new Cliente();
      let nombreCompleto = this.form.value['cliente'];
      this.cliente.nombreCompleto = nombreCompleto;
      this.cliente.dni = '0000000';
    }

    consumo.cliente = this.cliente;
    this.consumoService.registrarTransaccion(consumo, this.cliente).then(() => {
      this.emitirMensaje();
    });

  }

  emitirMensaje() {
    this.snackBar.open("Se registró exitosamente", 'AVISO', {
      duration: 2000,
    });

    setTimeout(() => {
      this.limpiar();
    }, 2000);
  }

  limpiar() {
    this.detalle = [];
    this.dataSource = new MatTableDataSource(this.detalle);
    this.cliente = new Cliente;
    this.total = 0;

    this.myControlCliente = new FormControl();
    this.myControlPlato = new FormControl();

    this.form = this.builder.group({
      'cliente': this.myControlCliente,
      'plato': this.myControlPlato,
      'fecha': new FormControl(new Date()),
      'cantidad': new FormControl(0),
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
