import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuario: string;
  ultimaConexion: string;
  desde: string;

  usuarioSocial: string;
  fotoSocial: string;

  constructor(public afa: AngularFireAuth) { }

  ngOnInit() {
    let currentUser = this.afa.auth.currentUser;
    
    this.usuario = currentUser.email;
    this.ultimaConexion = currentUser.metadata.lastSignInTime;
    this.desde = currentUser.metadata.creationTime;

    this.usuarioSocial = currentUser.displayName;
    this.fotoSocial = currentUser.photoURL;
  }

}
