import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { Plato } from './../../../_model/plato';
import { PlatoService } from './../../../_service/plato.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-plato-edicion',
  templateUrl: './plato-edicion.component.html',
  styleUrls: ['./plato-edicion.component.css']
})
export class PlatoEdicionComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject();

  form: FormGroup;
  id: string;
  edicion: boolean;

  labelFile: string;
  file: any;
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  urlImagen: string;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private platoService: PlatoService,
    private snackBar: MatSnackBar,
    private afStorage: AngularFireStorage,
    private afs: AngularFirestore
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      'id': new FormControl(''),
      'nombre': new FormControl(''),
      'precio': new FormControl(0)
    });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = this.id != null;
      this.initForm();
    });

  }

  initForm() {
    if (this.edicion) {
      this.platoService.leer(this.id).pipe(takeUntil(this.ngUnsubscribe)).subscribe((data: Plato) => {
        this.form = new FormGroup({
          'id': new FormControl(data.id),
          'nombre': new FormControl(data.nombre),
          'precio': new FormControl(data.precio)
        });
        if (data.id != null) {
          this.afStorage.ref(`platos/${data.id}`).getDownloadURL().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
            this.urlImagen = data;
          });
        }
      });
    }
  }

  operar() {
    let nuevoPlato = new Plato();
    nuevoPlato.id = this.id;
    nuevoPlato.nombre = this.form.value['nombre'];
    nuevoPlato.precio = this.form.value['precio'];
    let mensaje = '';

    if (this.edicion) {
      this.platoService.modificar(nuevoPlato);
      mensaje = 'SE MODIFICO';
    } else {
      nuevoPlato.id = this.afs.createId();
      this.platoService.registrar(nuevoPlato);
      mensaje = 'SE REGISTRO';
    }

    //se guarda la imagen en storage
    if (this.file != null) {
      this.ref = this.afStorage.ref(`platos/${nuevoPlato.id}`);
      this.task = this.ref.put(this.file);
      this.afStorage.ref
    }

    this.snackBar.open(mensaje, 'AVISO', {
      duration: 2000,
    });

    this.router.navigate(['plato']);
  }

  seleccionar(e: any) {
    this.file = e.target.files[0];
    this.labelFile = e.target.files[0].name;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
