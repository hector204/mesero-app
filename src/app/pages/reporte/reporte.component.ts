import { Component, OnInit } from '@angular/core';
import { ReporteService } from 'src/app/_service/reporte.service';
import * as moment from 'moment';
import { Chart } from 'chart.js';
import { FuncionService } from './../../_service/funcion.service';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})
export class ReporteComponent implements OnInit {

  chart: any;
  tipo: string;
  fechaSeleccionada: Date;

  constructor(
    private reporteService: ReporteService,
    private funcionService: FuncionService
  ) { }

  ngOnInit() {
    this.funcionService.probar();
  }


  cambiar(tipo: string) {
    this.tipo = tipo;
    if (this.chart) {
      this.chart.destroy();
    }
    this.dibujar();
  }

  dibujar() {
    this.reporteService.buscarPorFecha(this.fechaSeleccionada).subscribe((data: any) => {

      let fechas = data.map(res => moment(res.fechaPedido.toDate()).format('DD-MM-YYYY HH:mm:ss'));
      let total = data.map(res => res.total);

      this.chart = new Chart('canvas', {
        type: this.tipo,
        data: {
          labels: fechas,
          datasets: [
            {
              label: 'Fecha',
              data: total,
              borderColor: "#3cba9f",
              fill: false,
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 0, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 99, 132, 0.2)'
              ]
            }
          ]
        },
      });
    });
  }

}
