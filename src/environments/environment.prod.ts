export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCOcBflNiNh0nzx7XzzN2rWZ0MFBG4C4dM",
    authDomain: "mesero-app-demo-backend.firebaseapp.com",
    databaseURL: "https://mesero-app-demo-backend.firebaseio.com",
    projectId: "mesero-app-demo-backend",
    storageBucket: "mesero-app-demo-backend.appspot.com",
    messagingSenderId: "30622998450"
  }
};
